class Fib_number:
    def __init__(self):
        self.__current_index = 0
        self.__processed_numbers = {}
        self.__last_calculated_number = None
    
    def __init__(self, index):
        if index < 0:
            raise ValueError("No fibanachi numbers for negative indexes!")
        self.__current_index = index
        self.__processed_numbers = {}

    def __fib(self,index):
        if index in self.__processed_numbers:
            return self.__processed_numbers[index]
        if index <= 1:
            return index
        self.__processed_numbers[index] = self.__fib(index-1) + self.__fib(index-2)
        return self.__processed_numbers[index]

    def get_data(self):
        if self.__current_index in self.__processed_numbers:
            self.__last_calculated_number = self.__processed_numbers[self.__current_index]
            return self.__last_calculated_number
        self.__last_calculated_number = self.__fib(self.__current_index)
        return self.__last_calculated_number

    def print_result(self):
        print("Fibanachi number for index ",self.__current_index,"is",self.__last_calculated_number)
    
    def print_all_calculated_numbers(self):
        for idx in self.__processed_numbers:
            print("Fib(",idx,") =",self.__processed_numbers[idx])

    def set_index(self, idx):
        self.__current_index = idx
    
    def calculate_instantly(self, idx):
        self.__current_index = idx
        return self.get_data()
