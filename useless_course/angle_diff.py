#diff(0, 190)  # не 190, а 170, потому что 170 меньше
#Выше - пример из методического пособия... Я всегда думал, что разница между углом
#в ноль градусов и углом в 190 градусов это 190, ну или -170, то, что это просто 170 для меня шок
import math

def diff(theta_1,theta_2):
    d_theta = int(math.fabs(theta_1-theta_2))
    return min(d_theta,360 - d_theta)

if __name__ == "__main__":
    print(diff(0, 45))
    print(diff(0, 180))
    print(diff(0, 190))
    print(diff(280, 180))
