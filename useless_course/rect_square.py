from argparse import ArgumentParser


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Rectangle square calculation",prog="rect_square.py")
    arg_parser.add_argument("width", action="store",type=float)
    arg_parser.add_argument("height", action="store",type=float)
    args = arg_parser.parse_args()
    print("Square of rectangle with width of ",args.width,"and height of",args.height,"is",args.width*args.height)



