import math

def math_func(t,x):
    numerator = 9*math.pi*t + 10*math.cos(x)
    denominator = math.sqrt(t) - math.fabs(math.sin(t))
    if denominator == 0:
        raise ValueError("ERROR: Denomenator with x of",x,"and t of",t,"turns to null")
    return numerator/denominator*math.exp(x)


if __name__ == "__main__":
    t_ = None
    x_ = None
    while t_ is None or x_ is None:
        try:
            t_ = float(input("Enter the t var "))
            x_ = float(input("Enter the x var "))
        except ValueError:
            print("\"It is incorrect symbols provided by you!\nTry again you are free to\"\n The little mastered frog")
    try:
        print("The value of function on such var is",math_func(t_,x_))
    except ValueError as err:
        print(err.args[0])
