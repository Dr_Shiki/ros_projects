from fibanachi_class import Fib_number

if __name__ == "__main__":
    fib_inst = Fib_number(11)
    number = fib_inst.get_data()
    fib_inst.print_result()
    fib_inst.set_index(10)
    fib_inst.get_data()
    fib_inst.print_result()
    print(fib_inst.calculate_instantly(12))
    fib_inst.print_all_calculated_numbers()