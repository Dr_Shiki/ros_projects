

import math

class PathPoint:
    x = 0
    y = 0
    theta = 0
    def __init__(self, X, Y, Theta):
        self.x = X
        self.y = Y
        self.theta = Theta

    def __init__(self, X, Y):
        self.x = X
        self.y = Y
        self.theta = 1

    def set_theta(self, theta):
        self.theta = theta

    def print_it(self):
        print([self.x,self.y,self.theta])

class Figure2Draw:
    def __init__(self):
        self.cur_point = 0
        self.anchor_points = {}
        self.start_point = 0

    def __define_angle(self, p1, p2):
        return math.atan2(p1.y - p2.y, p1.x - p2.x)

    def add_point(self, point, order):
        self.anchor_points[order] = point

    def provide_theta(self):
        PathPoint.x
        if len(self.anchor_points) < 2:
            return
        for i in range(1, len(self.anchor_points)):
            self.anchor_points[i-1].set_theta(math.pi - self.__define_angle(self.anchor_points[i-1],self.anchor_points[i]))

        self.anchor_points[len(self.anchor_points)-1].set_theta(math.pi - self.__define_angle(self.anchor_points[len(self.anchor_points)-1],self.anchor_points[0]))

    def next_point(self):
        point = self.anchor_points[self.cur_point]
        self.cur_point +=1
        if self.cur_point >= len(self.anchor_points):
            self.cur_point = 0
        return point

    def print_points(self):
        for point in self.anchor_points:
            self.anchor_points[point].print_it()
    
    def point_at(self, idx):
        return self.anchor_points[idx]