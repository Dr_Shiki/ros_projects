
from math import sqrt, fabs, atan2
import math
import rospy
from turtlesim.msg import Pose
from geometry_msgs.msg import Twist
from enum import Enum

import final_work
from pid_controller import PID
from final_work.msg import RobotState, PathPoint

class State(Enum):
    WAITING = 0
    WAY_TO_POINT = 1
    ADJUSTING = 2
    FINISHED = 3

__LOG_LVL = 4

def log(msg, lvl):
    if lvl >= __LOG_LVL:
        print(msg)


heading_pid = PID(17.12, 0.1, 0.0, 3.4, 0.03)
long_pid = PID(0.8,0.0,0.0,30,0)

current_pos_ = None
target_pos_ = None
cur_state_ = State.WAY_TO_POINT

pose_sub_ = None
cmd_pub_ = None
state_pub_ = None
target_sub_= None

is_init = True
cur_point_num_ = 0

def distance(P1,P2):
    return sqrt(
        (P2[0]-P1[0])**2 +
        (P2[1]-P1[1])**2
    )

def on_pose_callback(msg):
    global current_pos_
    current_pos_ = [
        msg.x,
        msg.y,
        msg.theta
    ]
    log("Pose received",1)
    log(current_pos_,0)

def on_path_point_received(msg):
    global target_pos_
    target_pos_ = [
        msg.x,
        msg.y,
        msg.theta
    ]

def on_timer_ticked():
    global is_adjust
    global current_pos_
    global target_pos_
    global long_pid
    global heading_pid
    global is_init
    global cur_point_num_

    msg = RobotState()
    msg.cur_state = int(cur_point_num_)
    state_pub_.publish(msg)

    if cur_state_ == State.WAITING or target_pos_ == None:
        return
    
    state2pub = 0

    if cur_state_ == State.WAY_TO_POINT:
        go_to_point(current_pos_,target_pos_,long_pid,heading_pid)
    if cur_state_ == State.ADJUSTING:
        adjust(current_pos_,target_pos_,long_pid,heading_pid)
    if cur_state_ == State.FINISHED:
        finished()

def send_cmd(x,y,theta):
    msg = Twist()
    msg.linear.x = x
    msg.linear.y = y
    msg.angular.z = theta
    cmd_pub_.publish(msg)
    log("CMD transmitted",1)
    log(msg,0)

def go_to_point(pos,pos_star,s_pid,fi_pid):
    global cur_state_
    long_epsilon = 0.01
    heading_epsilon = 0.001
    
    s_err = distance(pos,pos_star)
    target_theta = atan2( -pos_star[1] + pos[1], -pos_star[0] + pos[0])
    theta_err = target_theta - pos[2]

    vel = s_pid.compute_control_cmd(s_err)
    ang_vel = fi_pid.compute_control_cmd(theta_err)

    if s_err < long_epsilon:
        vel = 0
        cur_state_ = State.ADJUSTING
    send_cmd(-vel,0,ang_vel)

def adjust(pos,pos_star,s_pid,fi_pid):
    global cur_state_
    long_epsilon = 0.1
    heading_epsilon = 0.0003
    s_err = distance(pos,pos_star)
    theta_err = (pos_star[2] - pos[2])%math.pi
    vel = 0
    ang_vel = s_pid.compute_control_cmd(theta_err)
    send_cmd(vel,0,ang_vel)
    if theta_err < heading_epsilon:
        cur_state_ = State.FINISHED
        
def finished():
    global cur_point_num_
    global target_pos_
    global cur_state_
    target_pos_ = None
    cur_state_ = State.WAY_TO_POINT
    cur_point_num_ +=1
    if cur_point_num_ > 5:
        cur_point_num_ = 1
    print("Point reached!")

if __name__ == '__main__':
    rospy.init_node('Controller',anonymous=True)
    
    pose_sub_ = rospy.Subscriber('/turtle1/pose',Pose,on_pose_callback,queue_size=1)
    cmd_pub_ = rospy.Publisher('/turtle1/cmd_vel',Twist,queue_size=1)
    state_pub_ = rospy.Publisher('/turtle_controller/state',RobotState,queue_size=1)
    target_sub_ = rospy.Subscriber('/turtle_controller/path',PathPoint, on_path_point_received, queue_size= 1)

    timer = rospy.Rate(20)
    while not rospy.is_shutdown():
        timer.sleep()
        on_timer_ticked()
