import math
import rospy


class PID:
    def __init__(self, kp, ki, kd, limit, i_limit):
        self.Kp = kp
        self.Ki = ki
        self.Kd = kd
        self.Limit = limit
        self.integral_limit = i_limit
        self.integral = 0.0
        self.last_err = 0.0
        self.last_time = 0
        self.d_err = 0.0


    def __diff_comp(self,error):
        d_err = (error-self.last_err)/(rospy.Time().now().to_nsec() - self.last_time)
        self.last_time = rospy.Time().now().to_nsec()
        self.last_err = error
        self.d_err = d_err

    def __integral_comp(self,error):
        self.integral += error
        if self.integral > self.integral_limit:
            self.integral = self.integral_limit * (math.fabs(self.integral)/self.integral)

    def compute_control_cmd(self, error):
        self.__diff_comp(error)
        self.__integral_comp(error)
        d_err = self.d_err
        int_err = self.integral

        cmd = self.Kp*error + self.Ki*int_err + self.Kd*d_err
        if math.fabs(cmd) > self.Limit:
            cmd = self.Limit*cmd/math.fabs(cmd)
        return cmd