import rospy
import final_work
import figure2draw
from figure2draw import Figure2Draw
from final_work.msg import PathPoint, RobotState

from final_work.srv import RectSize, RectSizeRequest


rect_size = 3

win_bounds = [
    [0,11],
    [11,11],
    [11,0],
    [0,0]
]

path_pub_ = None
state_sub_ = None
last_state_ = 1
target_point_ = None

figure_ = Figure2Draw()

def generate_anchor(bounds, rect_length):
    win_center = [
        (bounds[0][0]+bounds[1][0])/2,
        (bounds[0][1]+bounds[3][1])/2
    ]

    rect_half = rect_length/2
    return [
        [win_center[0]-rect_half, win_center[1]+rect_half],
        [win_center[0]+rect_half, win_center[1]+rect_half],
        [win_center[0]-rect_half, win_center[1]-rect_half],
        [win_center[0]+rect_half, win_center[1]-rect_half],
    ]

def generate_button_line_middle_point(rect):
    return [
        (rect[2][0]+rect[3][0])/2,
        (rect[2][1]+rect[3][1])/2
    ]

def prepare_figure(fig, bounds, length):
    anchor = generate_anchor(bounds, length)
    middle_point = generate_button_line_middle_point(anchor)
    
    def add_to_fig(point,order):
        fig.add_point(figure2draw.PathPoint(point[0],point[1]),order)
    
    add_to_fig(middle_point,0)
    add_to_fig(anchor[3],1)
    add_to_fig(anchor[1],2)
    add_to_fig(anchor[0],3)
    add_to_fig(anchor[2],4)
    add_to_fig(middle_point,5)
    fig.provide_theta()

def on_state_received(msg):
    next_point = figure_.point_at(msg.cur_state)
    _msg = PathPoint()
    _msg.x = next_point.x
    _msg.y = next_point.y
    _msg.theta = next_point.theta
    path_pub_.publish(_msg)
    rospy.sleep(rospy.Duration(1.1))


if __name__ == '__main__':
    node = rospy.init_node('planer',anonymous=True)
    prepare_figure(figure_, win_bounds, rect_size)
    figure_.print_points()

    path_pub_ = rospy.Publisher('/turtle_controller/path',PathPoint,queue_size=1)
    state_sub_ = rospy.Subscriber('/turtle_controller/state',RobotState,on_state_received,queue_size=1)

    rospy.spin()
    