import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import UInt8

node_name = 'rand_num_calculation'
pub_topic = '/result'
listen_topic = '/random_num_publisher/rand_num'


def create_random_number_cumulater(listen_topic_name, pub_topic_name, reset_count):
    counter_ = 0
    cumulative_ = 0
    def on_rand_num_received(msg,pub):
        nonlocal counter_
        nonlocal cumulative_
        nonlocal reset_count
        cumulative_ += msg.data
        counter_+= 1
        if counter_ >= reset_count:
            pub.publish(cumulative_)
            counter_ = 0
            cumulative_ = 0

    publisher_ = rospy.Publisher(pub_topic_name,UInt32,queue_size=2)
    reader_ = rospy.Subscriber(listen_topic_name,UInt8,on_rand_num_received,(publisher_),2)

if __name__ == '__main__':
    rospy.init_node(node_name,anonymous=True)
    print("Node "+ node_name+" has been initialized!")
    create_random_number_cumulater(listen_topic,pub_topic,5)
    rospy.spin()