import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import UInt8

node_name = 'rand_num_calculation'
pub_topic = '/rand_num/result'
listen_topic = '/rand_num/numbers'


def create_random_number_handler(listen_topic_name, pub_topic_name, reset_count):
    counter_ = 0
    avarage_ = 0
    def on_rand_num_received(msg,pub):
        nonlocal counter_
        nonlocal avarage_
        nonlocal reset_count
        avarage_ += msg.data
        counter_+= 1
        if not counter_ < reset_count:
            avarage_ /= reset_count
            pub.publish(int(avarage_))
            counter_ = 0
            avarage_ = 0

    publisher_ = rospy.Publisher(pub_topic_name,UInt32,queue_size=2)
    reader_ = rospy.Subscriber(listen_topic_name,UInt8,on_rand_num_received,(publisher_),2)

if __name__ == '__main__':
    rospy.init_node(node_name,anonymous=True)
    print("Node "+ node_name+" has been initialized!")
    create_random_number_handler(listen_topic,pub_topic,5)
    rospy.spin()