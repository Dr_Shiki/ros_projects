import rospy
from rospy.core import is_shutdown
from std_msgs.msg import UInt8

import random

node_name = 'rand_num_publisher'
topic_name = '/rand_num/numbers'

def num_publish(publisher):
    msg = UInt8()
    msg.data = int((random.random()*98)+1)
    publisher.publish(msg)

if __name__ == '__main__':
    rospy.init_node(node_name,anonymous=True)
    print("Node "+ node_name+" has been initialized!")
    pub_ = rospy.Publisher(topic_name,UInt8,queue_size=1)
    ros_timer_ = rospy.Rate(5)
    print("Node is running")
    while(not rospy.is_shutdown()):
        num_publish(pub_)
        ros_timer_.sleep()