#include <ros/ros.h>
#include <std_msgs/String.h>

void on_string_received(std_msgs::String msg)
{
    ROS_INFO(msg.data.data());
}

int main(int argc,char** argv)
{
    ros::init(argc,argv,"TestNode");
    ros::NodeHandlePtr node_handle(new ros::NodeHandle(""));
    ros::Subscriber _sub = node_handle->subscribe("/str2print",1,&on_string_received);
    ros::spin();
    ROS_INFO("Hello world!");
    return 0;
}