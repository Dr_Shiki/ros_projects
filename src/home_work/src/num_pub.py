import rospy
from rospy.core import is_shutdown
from std_msgs.msg import UInt8

import random

__GREEN_C = '\033[92m'
__END_C = '\033[0m'

node_name = 'rand_num_publisher'

def num_publish(publisher):
    msg = UInt8()
    msg.data = int((random.random()*98)+1)
    publisher.publish(msg)
    #print(__GREEN_C+node_name+": "+"Msg published:",msg,__END_C)

if __name__ == '__main__':
    rospy.init_node(node_name,anonymous=True)
    topic_name = rospy.get_param('rand_num_topic','/rand_num/numbers')

    print(__GREEN_C)
    print("Node "+ node_name+" has been initialized!")
    print("Publish in topic",topic_name)
    
    pub_ = rospy.Publisher(topic_name,UInt8,queue_size=1)
    ros_timer_ = rospy.Rate(5)
    print("Node is running")
    print(__END_C)

    while(not rospy.is_shutdown()):
        num_publish(pub_)
        ros_timer_.sleep()