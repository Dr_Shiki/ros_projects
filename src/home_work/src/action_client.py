import rospy
import actionlib

from home_work.msg import FibonacciAction, FibonacciGoal


def on_feedback(res):
    print('sequence:',list(res.sequence))

def on_done(status, res):
    print('Have final result:')
    print('sequence',list(res.sequence))

def fibonacci_client():
    client = actionlib.SimpleActionClient('fibonacci', FibonacciAction)
    client.wait_for_server()
    goal = FibonacciGoal(order=6)
    client.send_goal(goal,on_done,feedback_cb= on_feedback)
    client.wait_for_result()
    return client.get_result()


if __name__ == '__main__':
    rospy.init_node('fibonacci_client')
    result = fibonacci_client()