import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import UInt8

node_name = 'rand_num_calculation'

__RED_C = '\033[91m'
__END_C = '\033[0m'


def create_random_number_handler(listen_topic_name, pub_topic_name, reset_count):
    counter_ = 0
    avarage_ = 0
    def on_rand_num_received(msg,pub):
        nonlocal counter_
        nonlocal avarage_
        nonlocal reset_count
        avarage_ += msg.data
        counter_+= 1
        #print(__RED_C+node_name+": "+'Data received',msg.data,__END_C)
        if not counter_ < reset_count:
            avarage_ /= reset_count
            pub.publish(int(avarage_))
            #print(__RED_C+node_name+": "+'Msg published',int(avarage_),__END_C)
            counter_ = 0
            avarage_ = 0
            

    publisher_ = rospy.Publisher(pub_topic_name,UInt32,queue_size=2)
    reader_ = rospy.Subscriber(listen_topic_name,UInt8,on_rand_num_received,(publisher_),2)

if __name__ == '__main__':
    rospy.init_node(node_name,anonymous=True)
    listen_topic = rospy.get_param('rand_num_topic','/rand_num/numbers')
    pub_topic = rospy.get_param('/seq_topic','/rand_num/result')
    some_msg = rospy.get_param('~msg_idk',default='Cannot get from param srv')

    print(__RED_C)
    print("Node "+ node_name+" has been initialized!")
    print('Listen on '+listen_topic)
    print('Publish in '+pub_topic)
    print('MSG from launch',some_msg)
    print(__END_C)

    create_random_number_handler(listen_topic,pub_topic,5)
    rospy.spin()