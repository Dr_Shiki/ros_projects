import rospy

from std_srvs.srv import SetBool, SetBoolResponse

class ColoredStr:
    __ON_C = '\033[92m'
    __OFF_C = '\033[91m'
    __END_C = '\033[0m'
    def Green(string):
        return ColoredStr.__ON_C + string + ColoredStr.__END_C
    
    def Red(string):
        return ColoredStr.__OFF_C + string + ColoredStr.__END_C

class Heater:

    def __init__(self):
        self.cur_state = False

    def State(self):
        msg = ColoredStr.Green("On") if self.cur_state else ColoredStr.Red("Off")
        print('Heater is '+msg)
    def Switch(self, state):
        if self.cur_state == state:
            return False
        self.cur_state = state
        self.State()
        return True



node_name = 'heater_server_node'
obj_ = Heater()

def req_handler(req):
    res = obj_.Switch(req.data)
    return SetBoolResponse(True,'State has been changed' if res else 'State has not been changed')


if __name__ == "__main__":
    print(ColoredStr.Green("Heater server is Up"))
    rospy.init_node(node_name)
    srv = rospy.Service('heater',SetBool,req_handler)
    rospy.spin()
    print(ColoredStr.Green("Heater server is Down"))
    



