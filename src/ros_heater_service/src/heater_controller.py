import pickle
import rospy

import sys

from std_srvs.srv import SetBoolRequest, SetBool
from std_msgs.msg import Int16

node_name = 'heater_controller'
ref_topic = '/temp'
temp_param = ['/low_temp','/hight_temp']


def request_heater_switch(state):
    try:
        set_heat = rospy.ServiceProxy('heater',SetBool)
        res = set_heat(state)
        return res.success, res.message
    except rospy.ServiceException as err:
        print('Request cannot be resolved by server')
    
def on_temp_topic(msg, limits):
    cur_temp = msg.data
    print("T = %d" % cur_temp)
    if cur_temp < limits[0]:
        print("Trying to turn on heater")
        (flg, mess) = request_heater_switch(True)
        if flg:
            print(mess)
        else:
            print("Error in response")
    if cur_temp > limits[1]:
        print("Trying to turn off heater")
        (flg, mess) = request_heater_switch(False)
        if flg:
            print(mess)
        else:
            print("Error in response")

if __name__ == '__main__':
    rospy.init_node(node_name)

    switching_limits = [rospy.get_param(temp_param[0],18), rospy.get_param(temp_param[1],23)]

    print('Controller run with temp backlash =',switching_limits)
    
    print('Awaiting server')
    rospy.wait_for_service('heater')
    print('Server online')

    sub_ = rospy.Subscriber(ref_topic,Int16,on_temp_topic,(switching_limits),1)
    rospy.spin()